﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Watermark
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "Click on 'Watermark PDF' button to start watermarking.. ";
            
        }      
        private string CalculateDateNJavaScript()
        {

            DateTime today = DateTime.Now;
            today = today.AddDays(50);

            string expiryDate = "var LastDay = "+today.Day+"; var LastMonth = "+today.Month+"; var LastYear = "+today.Year+"; " +
                       " var today = new Date(); " +
                       " var myDate=new Date(); " +
                       " LastMonth = LastMonth - 1; " +
                       " myDate.setFullYear(LastYear,LastMonth,LastDay); " +
                       " if (myDate<today) " +
                       " {app.alert('This document has expired. Please contact your document provider.');this.closeDoc(1); } ";

            return expiryDate;

        }

       
         /// <summary>
        /// do watermarking single by single process
        /// </summary>
        /// <param name="FileLocation"></param>
        /// <param name="WatermarkFileLocation"></param>
        private void Watermark(string FileLocation, string WatermarkFileLocation)
        {
            PdfReader pdfReader = new PdfReader(FileLocation);
            try
            {             
                 // watermark file 
                using (FileStream fs = new FileStream(WatermarkFileLocation, FileMode.Create, FileAccess.Write, FileShare.None))
                {    
                    PdfStamper stamp = new PdfStamper(pdfReader, fs);

                    //Add Expiry Date and add javascript into PDF file
                    string expiryDate = CalculateDateNJavaScript();
                    stamp.AddJavaScript("Expiry", expiryDate);

                    PdfContentByte waterMark;
                    string text = "For DOD Internal Use ONLY";

                    for (int page = 1; page <= pdfReader.NumberOfPages; page++)
                    {
                        waterMark = stamp.GetOverContent(page);
                        //font size and name 
                        waterMark.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 22);

                        waterMark.SetColorFill(BaseColor.RED);
                        //indicates start of writing of text
                        waterMark.BeginText();
                        // 300 means text alignment and 10 means height from bottom
                        waterMark.ShowTextAligned(1, text, 300, 10, 0);

                        waterMark.EndText();
                    }
                    stamp.FormFlattening = true;
                    stamp.Close();
                    pdfReader.Close();
                    //delete original file and move watermark frile to original file place
                    File.Delete(FileLocation);
                    File.Move(WatermarkFileLocation, FileLocation);
                }

            }
               
            catch (Exception ex)
            {
                string fileName = "../../LogFile.txt";
                label1.Text = "Error is logged into log file and continue for other file";
                label1.Update();
                CreateLogFile(ex.Message + ex.StackTrace + Environment.NewLine + FileLocation + Environment.NewLine, fileName);
                
            }
            finally
            {
                pdfReader.Dispose();
               
            }
        }
        /// <summary>
        /// creating log file if does not exist and write error into log file.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="fileName"></param>
        private void CreateLogFile(string errorMessage, string fileName)
        {
            // Check if file already exists. If yes, delete it. 
            if (!File.Exists(fileName))
            {
                // Create a new file 
                using (FileStream w = File.Create(fileName))
                {
                    Byte[] title = new UTF8Encoding(true).GetBytes(errorMessage + Environment.NewLine);
                    w.Write(title, 0, title.Length);

                }
            }
            else
            {   //updating existing file
                using (StreamWriter w = File.AppendText(fileName))
                {
                    w.WriteLine("--------" + DateTime.Now + "----------");
                    w.WriteLine(errorMessage);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                label1.Text = "Watermark process is started.. ";
                label1.Update();
                button1.Enabled = false;

                string FileLocation = System.Configuration.ConfigurationSettings.AppSettings["DestinationLocation"]; //"P:\\Destination\\";
                string WatermarkLocation = System.Configuration.ConfigurationSettings.AppSettings["DestinationWatermarkLocation"]; //"P:\\Destination\\";
                //Get all pdf files from destination path"
                var filenames = Directory.EnumerateFiles(FileLocation, "*.pdf", SearchOption.AllDirectories);
                //loop all files 
                label1.Text = "Watermark process is in progress.. ";
                label1.Update();
                foreach (string fileName in filenames)
                {                   
                    // calling watermark function
                    Watermark(fileName, WatermarkLocation + "\\" + Path.GetFileName(fileName));
                   
                }
                label1.Text = "Watermark process is completed.. ";
                button1.Enabled = true;
            }
            catch (Exception ex)
            {
                string fileName = "../../LogFile.txt";
                label1.Text = "Error is logged into log file";
                button1.Enabled = true;
                CreateLogFile(ex.Message + Environment.NewLine + ex.StackTrace, fileName);
            }
        }    
    }
}
