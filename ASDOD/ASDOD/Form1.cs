﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace ASDOD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //basic HTML Top part
        private StringBuilder CreateTable(StringBuilder sb)
        {
            sb.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd \"> \r\n ");
            sb.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
            sb.Append("<head>\r\n");
            sb.Append("<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />\r\n");
            sb.Append("<title>Defence Library Service Home Page</title>\r\n");
            sb.Append("</head>\r\n");
            sb.Append("<body>\r\n");
            sb.Append("<table class='c3' width='100%' border='0'><tr><td class='c1'><div class='c1'>  <p class='linkname'><a href='http://caas/Areas/CORP/Library/default.aspx'>Home</a> |<a href='http://intranet.defence.gov.au/'>Defweb</a> | <a href='http://library.defence.gov.au/standards/standards.nsf/$$ViewTemplateDefault'>Standards " +
              "Australia Search</a> </p></div></td> </tr></table><table class='c3' width='100%' border='0'><tr><td><table class='c3'><tr><td>Defence Library Service Standards Australia Fulltext Online</td></tr>" +
              "<tr><td class='c1'><p class='heading'>");
            return sb;
        }
       //creating robocopy command
        private void CreateTXT(string fileName, string Source, string Destination, string FolderName, string ActualPDFPATH)
        {
            // Check if file already exists. If yes, delete it. 
            if (!File.Exists(fileName))
            {
                // Create a new file 
                using (FileStream w = File.Create(fileName))
                {
                    Byte[] title = new UTF8Encoding(true).GetBytes("xcopy  " + "\"" + Source + ActualPDFPATH + "\" " + "\"" + Destination + "\\Index\\PDFs" + FolderName + "\\" + "\" " + " /Y /I /S >>log.txt 2>>error.txt" + Environment.NewLine);
                    w.Write(title, 0, title.Length);

                }
            }
            else
            {   //updating existing file
                using (StreamWriter w = File.AppendText(fileName))
                {
                    w.WriteLine("xcopy  " + "\"" + Source + ActualPDFPATH + "\" " + "\"" + Destination + "\\Index\\PDFs" + FolderName + "\\"+"\" "+" /Y /I /S >>log.txt 2>>error.txt");
                    //xcopy  "C:\ASDOD_DEC\Pdfs\AS2\7.2 -2008_R2017.pdf" "C:\ASDOD_DEC\AS2\" /Y /I /S >> r.txt

                }
            }
        }
        //creating robocopy command
        private void CreateLogFile(string errorMessage,string fileName)
        {
            // Check if file already exists. If yes, delete it. 
            if (!File.Exists(fileName))
            {
                // Create a new file 
                using (FileStream w = File.Create(fileName))
                {
                    Byte[] title = new UTF8Encoding(true).GetBytes(errorMessage + Environment.NewLine);
                    w.Write(title, 0, title.Length);

                }
            }
            else
            {   //updating existing file
                using (StreamWriter w = File.AppendText(fileName))
                {
                    w.WriteLine("--------"+DateTime.Now+"----------");
                    w.WriteLine(errorMessage);
                    //xcopy  "C:\ASDOD_DEC\Pdfs\AS2\7.2 -2008_R2017.pdf" "C:\ASDOD_DEC\AS2\" /Y /I /S >> r.txt

                }
            }
        }
        private void CreateHTML()
        {
            try
            {
                string Destination = System.Configuration.ConfigurationSettings.AppSettings["DestinationLocation"];
                string Source = System.Configuration.ConfigurationSettings.AppSettings["SourceLocation"];
                Directory.CreateDirectory(Destination + "\\HTML");
                Directory.CreateDirectory(Destination + "\\TXT");

                Directory.CreateDirectory(Destination + "\\Image");//Create Image folder

                CreateImageFolder(Destination + "\\Image");// copying static files to destination image folder 

                StringBuilder sb = new StringBuilder();

                sb = CreateTable(sb);// create basic table

                lblMessage.Text = "Connecting to Database.";
                DataSet ds = GetData();  //Get data from database
                lblMessage.Text = "Got the Data.";
                lblMessage.Update();
                int count = ds.Tables[0].Rows.Count;

                lblMessage.Text = "Started creating HTML file. It will take time 2 to 5 mins.";
                lblMessage.Update();
                //System.Threading.Thread.Sleep(1000);

                int i = 0;
                foreach (DataRow dr in ds.Tables[0].Rows) // this is where you run through the dataset and get values you want from it.
                {

                    sb.Append(dr["TITL"].ToString());

                    //create destination path for HTML files..
                    int countBackSlash = dr["ActualPDFPATH"].ToString().Count(f => f == '\\');
                    string createDestinationHTML = string.Empty;
                    for (int j = 0; j < countBackSlash; j++)
                    {
                        createDestinationHTML += "../";

                    }
                    sb.Append("</td></tr></table><td> </td></td></tr></table><p><div class='notice'>Click title to open document &nbsp;&nbsp;<B><A HREF='" + createDestinationHTML.Replace(@"\\", "/") + "index/pdfs" + dr["ActualPDFPATH"].ToString().Replace(@"\", "/") + "'>" + dr["DEGN"].ToString() + "&nbsp;" + dr["TITL"].ToString() + "</A></B><br></div><B>Status :&nbsp;</B>");
                    sb.Append(dr["STATUS"].ToString() + "<B><br>Synopsis :&nbsp;</B>");

                    sb.Append(dr["ABSTRACT"].ToString() + dr["Note"].ToString() + "<br><B>ISBN :&nbsp;</B>" + dr["ISBN"].ToString() + "<br><B>Pages :&nbsp;</B>");
                    sb.Append(dr["PAGES"].ToString());

                    sb.Append("<br><h5 align='center'>Copyright © 2001 Standards Australia/Commonwealth of Australia<br>Comments to <a href='mailto:defence.library@defence.gov.au'>DLS Webmaster</a><br> " +
                              "<a href='http://intranet.defence.gov.au/dmoweb/sites/copyright/ComWeb.asp?page=127474'>Disclaimer</a><br> Generated on " + DateTime.Now.ToString("dd/MM/yyyy") + "</h5>");
                    sb.Append("</body>\r\n");
                    sb.Append("</html>\r\n");

                    //creating HTML and write data in HTML
                    Directory.CreateDirectory(Destination + "\\HTML" + dr["FolderName"].ToString());
                    Directory.CreateDirectory(Destination + "\\Index\\PDFs");//Create Index folder for Pdf files


                    using (FileStream fs = new FileStream(Destination + "\\HTML" + dr["PDFPATH"].ToString(), FileMode.Create))
                    {
                        using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                        {

                            w.WriteLine(sb.ToString());
                            sb.Clear();
                            sb = CreateTable(sb);
                        }
                    }
                    string filename = Destination + "\\TXT\\CopyCommand.txt";
                    CreateTXT(filename, Source, Destination, dr["FolderName"].ToString(), dr["ActualPDFPATH"].ToString());

                    i++;
                    if (count == i)// finally all done..
                    {
                        lblMessage.Text = "Completed Process. Please verify all folders and Run CopyCommand to copy pdf files.";
                        lblMessage.Update();
                        btnSubmit.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        private void CreateImageFolder(string targetPath)
        {
            string sourcePath = "../../Image";
            string fileName, destFile;
            

            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files and overwrite destination files if they already exist.
                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(targetPath, fileName);
                    System.IO.File.Copy(s, destFile, true);
                }
            }
            else
            {
                Console.WriteLine("Source path does not exist!");
            }

        }
       private DataSet GetData()
       {

           string connetionString = null;
           SqlConnection connection;
           SqlDataAdapter adapter = new SqlDataAdapter();
           SqlCommand cmd=new SqlCommand();
           DataSet ds = new DataSet();

           connetionString = System.Configuration.ConfigurationSettings.AppSettings["Connection"].ToString();// "data source=auhdc1-isqsql03\\sql2;initial catalog=saa_catalogue;integrated security=true;Persist Security Info=True;multipleactiveresultsets=True";
           //System.Configuration.ConfigurationSettings.AppSettings["saaCatalogueEntities"];
           connection = new SqlConnection(connetionString);
           try
           {
                connection.Open();
             
                cmd = new SqlCommand("GetASDODData", connection);                
                cmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);
                connection.Close();
              
           }
           catch (Exception ex)
           {
               //MessageBox.Show(ex.ToString());
               throw ex;
               
           }
            return ds;
       }
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                btnSubmit.Enabled = false;
                #region robocopy
                //RoboCommand backup = new RoboCommand();
                //// events
                ////// backup.OnFileProcessed += backup_OnFileProcessed;
                //// backup.OnCommandCompleted += backup_OnCommandCompleted;
                //// copy options
                //backup.CopyOptions.Source = txtSource.Text;//"U:\\New Folder";
                //backup.CopyOptions.Destination = txtdestination.Text +"\\Index\\PDFs" ;// "U:\\Geeta1";
                //backup.CopyOptions.CopySubdirectories = true;
                //backup.CopyOptions.UseUnbufferedIo = true;

                //// select options
                ////  backup.SelectionOptions.OnlyCopyArchiveFilesAndResetArchiveFlag = true;

                //// retry options
                //backup.RetryOptions.RetryCount = 1;
                //backup.RetryOptions.RetryWaitTime = 2;
                //backup.Start();
                //Thread.Sleep(1000);
                //MessageBox.Show("Files has been  copied successfully. ");
                #endregion

                //Creating HTML
                CreateHTML();
            }
            catch(Exception ex)
            {
               // MessageBox.Show(e../../Imagex.Message);
                string fileName = "../../LogFile.txt";
                lblMessage.Text = "Error is logged into log file";
                btnSubmit.Enabled = true;
                CreateLogFile(ex.Message+Environment.NewLine+ex.StackTrace, fileName);
            }

        }

       
    }
}
